module ApplicationHelper

  def welcome_text
    str = "" #jika user telah login, tampilkan welcome textnya 
    if current_user 
      str = "Welcome, #{current_user.email}"
    else 
      str += link_to "Signup", sign_up_path 
    end 
  end

end
