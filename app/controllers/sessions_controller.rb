class SessionsController < ApplicationController

  def new
  end
  
  def create
   user = User.authenticate(params[:email], params[:password])
   if user
     session[:user_id] = user.id
     if current_user.is_admin?
       redirect_to admin_categories_path
     else
       redirect_to user_path(user.id), :method => :get 
     end
    else
      flash[:error] ="Invalid email or password"
      redirect_to :root
    end
  end

  def destroy
   session[:user_id] = nil
   redirect_to :root
  end

end
