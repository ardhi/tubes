class HomeController < ApplicationController

  def index
    @articles = Article.all
    @products = Product.all
  end
  
end
