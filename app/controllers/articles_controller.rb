class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:create, :new, :edit, :update, :destroy]
  before_filter :find_article, :only => [:edit, :update, :show, :destroy]
  
  def new
    @article = Article.new
  end
  
  def create
    @article = Article.create(params[:article])
    if @article.save
      flash[:notice] = "Success save into database"
      redirect_to articles_path
        else
          flash[:notice] = "Failed save into database"
          render :action => :new
        end
  end
  
  def edit
  end
  
  def update
    if @article.update_attributes(params[:article])
      flash[:notice] = "Success update and save into database"
      redirect_to articles_path
        else
          flash[:notice] = "failed to update and save into database"
          render :action => edit
        end
  end

  def index
    @articles = Article.all
  end

  def show
    @user = User.find(@article.user_id)
    @comment = Comment.new
    @comments = Comment.where("article_id = #{params[:id]}")
  end
  
  def destroy
    @article.destroy
    flash[:notice] = "Success delete from database"
    redirect_to articles_path
  end
  
  def find_article
    @article = Article.find(params[:id])
  end
  
end
