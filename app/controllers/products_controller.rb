class ProductsController < ApplicationController
  before_filter :require_login, :only => [:create, :new, :edit, :update, :destroy]
  before_filter :find_product, :only => [:edit, :update, :destroy, :show]
  before_filter :find_category, :only => [:new, :create, :edit, :update]
  
  def show
    @user = User.find(@product.user_id)
  end
  
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end
  
  def create
    @product = Product.create(params[:product])
    if @product.save
      flash[:notice] = "success save into database"
      redirect_to :root
    else
      flash[:error] = "Please try again"
      render :action => :new
    end
  end
    
  def edit
  end
  
  def update
    if @product.update_attributes(params[:product])
      flash[:notice] = "Success for update and save into database"
      redirect_to products_path
      else
        flash[:error] = "failed for update"
        render :action => :edit
      end
  end
  
  def destroy
    @product.destroy
    flash[:notice] = "success for delete"
    redirect_to products_path
  end
  
  def find_product
    @product = Product.find(params[:id]) 
  end
  
  def find_category
    @parent_category = Category.where("parent_id IS not null").map{|c| [c.name, c.id]}
  end
  
end
