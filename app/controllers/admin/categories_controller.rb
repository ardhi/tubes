class Admin::CategoriesController < Admin::ApplicationController
  before_filter :require_admin_login
  before_filter :find, :only => [:show, :edit, :update, :destroy]
  def index
    @categories = Category.all
  end
  
  def show
  end
  
  def new
    @category = Category.new
    @parent_category = Category.where("parent_id IS null").map{|c| [c.name, c.id]}
  end

  def create
    @category = Category.create(params[:category])
    if @category.save
      flash[:notice] = "Success save into database"
      redirect_to admin_categories_path
        else
          flash[:notice] = "Please try Again"
          render :action => :new
        end
  end
  
  def edit
    @parent_category = Category.where("parent_id IS null").map{|c| [c.name, c.id]}
  end
  
  def update
    if @category.update_attributes(params[:category])
      flash[:notice] = "Success Update and save into database"
      redirect_to admin_categories_path
        else
          flash[:notice] = "please try again to update"
          render :action => :edit
        end
  end
  
  def destroy
    @childcategory = Category.categorychild(params[:id])
    @childcategory.each do |cat|
      cat.destroy
    end
    
    @category.destroy
    flash[:notice] = "Success delete data from database"
    redirect_to admin_categories_path
  end
  
  def find
    @category = Category.find(params[:id])
  end
    
end
