class UsersController < ApplicationController
  
  before_filter :find_user, :only => [:show, :edit, :update]
  
  def index
  end

  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  def create
    @user = User.new(params[:user])
      if verify_recaptcha
        if @user.save
          UserMailer.registration_confirmation(@user).deliver
          flash[:notice] = "Success save into database"
          redirect_to user_path(@user.id), :method => :get
        else
          flash[:error] = "faild save into database"
          render :action => :new
        end
      else
      flash[:error] = "There was an error with the recaptcha code below.
                     #Please re-enter the code and click submit."
      render :action => :new
      end
  end

  # PUT /users/1
  def update
      if @user.update_attributes(params[:user])
        flash[:notice] = "Success update and save into database"
        redirect_to user_path(@user.id), :method => :get
      else
        flash[:error] = "Failed update"
        render :action => :edit
      end
  end

  def destroy
  end
  
  def find_user
    @user = User.find(params[:id])
  end
  
end
