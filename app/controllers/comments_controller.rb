class CommentsController < ApplicationController
  
  def create
    #@article = Article.find(params[:article_id])
    @comment = Comment.new(params[:comment])
    @user = User.find(@comment.user_id)
    #redirect_to article_path(@article)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_path(params[:comment][:article_id]), :notice => 'Comment was successfully created.') }
        format.js
      end
    end
  end
  
end
