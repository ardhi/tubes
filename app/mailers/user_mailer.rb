class UserMailer < ActionMailer::Base
  
  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :subject => "Registered", :from => 'training.ror@executionisqueen.com', :content_type => 'text/plain')
  end
end
