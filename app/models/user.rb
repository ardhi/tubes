class User < ActiveRecord::Base
  attr_accessible :id, :name, :email, :address, :password, :password_confirmation, :gender , :birthday, :phone_number, :facebook, :blog
  attr_accessor :password
  has_many :products
  before_save :encrypt_password

  validates :name, :presence => true,
      	           :length => {:minimum => 3, :maximum => 20}

  validates :email, :presence => true,
	                  :uniqueness => true,
	                  :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}

  validates :password, :presence => {:on => :create},
                       :confirmation => true
  
  validates :phone_number, :uniqueness => true
  
  validates :gender, :presence => true                  
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def is_admin?
    if self.email == "ian_bowo44@yahoo.com"
      return true
    else
      return false
    end  
  end
  
end
