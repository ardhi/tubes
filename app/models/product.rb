class Product < ActiveRecord::Base
  attr_accessible :id, :name, :description, :price, :weight, :user_id, :category_id
  belongs_to :user
  has_one :category
  
  validates :name, :presence => true,
      	           :length => {:minimum => 3, :maximum => 20}
  
  validates :description, :presence => true,
                          :length => {:minimum =>6}
  
  validates :category_id, :presence => true

end
