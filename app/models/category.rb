class Category < ActiveRecord::Base
  belongs_to :product
  
  attr_accessible :id, :name, :parent_id
  
  validates :name, :presence => true,
                   :length => {:minimum => 3},
                   :uniqueness => true
  
  scope :categorychild, lambda { |parent_id| where("parent_id = ?", parent_id) }
  
end
