class Article < ActiveRecord::Base
  attr_accessible :id, :title, :body, :user_id
  has_many :comments, :dependent => :destroy
  
    validates :title, :presence => true,
                      :length => {:minimum => 3},
                      :uniqueness => true
    
    validates :body, :presence => true
  
end
